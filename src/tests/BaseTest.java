package tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTest {

	public static WebDriver driver = null;
	public static WebDriverWait driverWait = null;

	@BeforeSuite
	public void initialize() throws IOException {
		DesiredCapabilities caps = new DesiredCapabilities();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-popup-blocking");
		options.addArguments("--disable-notifications");
		caps.setCapability(ChromeOptions.CAPABILITY, options);

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
		driver = new ChromeDriver(caps);
		driverWait = new WebDriverWait(driver, 30L);
		// To maximize browser
		driver.manage().window().maximize();
		// Implicit wait
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		// To open facebook
		// driver.get("https://www.facebook.com");

	}

	@AfterSuite
	// Test cleanup
	public void TeardownTest() {
		BaseTest.driver.quit();
	}

	public static void browserWait(Long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This method enters values into the fields
	 * 
	 * @param ele
	 * @param value
	 */
	public static void fillField(WebElement ele, String value) {
		if (ele.isDisplayed()) {

			ele.sendKeys(value);
		} else {
			Assert.fail("Field " + ele + " is not displayed/enabled");
		}
	}

	/**
	 * This method enters values into the fields
	 * 
	 * @param ele
	 * @param key
	 */
	public static void fillField(WebElement ele, Keys key) {
		if (ele.isDisplayed()) {

			ele.sendKeys(key);
		} else {
			Assert.fail("Field " + ele + " is not displayed/enabled");
		}
	}
	
	
	/**
	 * This method enters values into the fields
	 * 
	 * @param ele
	 */
	public static void clickElement(WebElement ele) {
		if (ele.isDisplayed()) {
			BaseTest.driverWait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			Reporter.log("Given link"+ele+" clicked ");
		} else {
			Assert.fail("Field " + ele + " is not displayed/enabled");
		}
	}
}
