package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.CirclesLifeHomePage;
import pages.CirclesLifeMyAccountPage;
import pages.CirclesLifeSignUpPage;
import pages.CirclesLifeWebPage;

public class CirclesLifeTest {

	@Test
	@Parameters({ "firstName", "lastName", "email", "pwd"})
	public void circleAccountTest(String firstName, String lastName, String email, String pwd) throws Exception {

		BaseTest.driver.get("https://pages.circles.life/");
		CirclesLifeWebPage circlePage = PageFactory.initElements(BaseTest.driver, CirclesLifeWebPage.class);
		CirclesLifeSignUpPage signUpPage = PageFactory.initElements(BaseTest.driver, CirclesLifeSignUpPage.class);
		CirclesLifeHomePage homePage = PageFactory.initElements(BaseTest.driver, CirclesLifeHomePage.class);
		CirclesLifeMyAccountPage AcctPage = PageFactory.initElements(BaseTest.driver, CirclesLifeMyAccountPage.class);
		circlePage.clickSignUp();
		signUpPage.fillCreateAcctForm(firstName, lastName, email, pwd);
		homePage.clickMyAccount();
		AcctPage.validateAcctEmail(email);
	}
}
