package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.FaceBookHomePage;
import pages.FaceBookLoginPage;

public class FaceBookLoginTest {

	@Test
	@Parameters({ "email", "pwd","comment"})
	public void facebookTest(String email, String pwd, String comment) throws Exception{

			BaseTest.driver.get("https://www.facebook.com");
			FaceBookLoginPage loginpage = PageFactory.initElements(BaseTest.driver, FaceBookLoginPage.class);
			FaceBookHomePage homepage = PageFactory.initElements(BaseTest.driver, FaceBookHomePage.class);
			loginpage.setEmail(email);
			loginpage.setPassword(pwd);
			loginpage.clickOnLoginButton();
			homepage.clickOnHomeLink();
			homepage.clickOnCommentLink();
			homepage.enterComments(comment);
			homepage.validateComment(comment);
		}
	
}
