package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Reporter;

import tests.BaseTest;

public class FaceBookLoginPage {

	WebDriver driver;

	public FaceBookLoginPage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@type='email'][@name='email']")
	WebElement emailTextBox;
	@FindBy(how = How.XPATH, using = "//input[@type='password'][@name='pass']")
	WebElement passwordTextBox;
	@FindBy(how = How.ID, using = "loginbutton")
	WebElement signInButton;

	// Defining all the user actions (Methods) that can be performed in the Facebook
	// home page

	// This method is to set Email in the email text box
	public void setEmail(String strEmail) {
		BaseTest.fillField(emailTextBox, strEmail);
	}

	// This method is to set Password in the password text box
	public void setPassword(String strPassword) {
		BaseTest.fillField(passwordTextBox, strPassword);
	}

	// This method is to click on Login Button
	public void clickOnLoginButton() {
		BaseTest.clickElement(signInButton);
		Reporter.log("Logged in successfully");
	}

}
