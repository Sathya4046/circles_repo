package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import tests.BaseTest;

public class CirclesLifeMyAccountPage {
	WebDriver driver;

	public CirclesLifeMyAccountPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//label[text()='Email']/parent::div/div/input")
	WebElement acctEmail;

	/**
	 * This method validates the email used in the Account creation 
	 * and the one displayed in My profile
	 * @param email
	 */
	public void validateAcctEmail(String email) {
		BaseTest.driverWait.until(ExpectedConditions.urlContains("https://shop.circles.life/my_profile"));
		Assert.assertEquals(acctEmail.getAttribute("value"), email.toLowerCase());
		Reporter.log("My account email id validated successfully");
	}

}
