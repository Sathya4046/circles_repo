package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

import tests.BaseTest;

public class CirclesLifeHomePage {

	WebDriver driver;

	public CirclesLifeHomePage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//a[@class='Links']/div[text()='MY ACCOUNT']")
	WebElement myAccountLink;

	/**
	 * Method to click the My Account menu item
	 */
	public void clickMyAccount() {
		BaseTest.driverWait.until(ExpectedConditions.urlContains("https://shop.circles.life/plan"));
		BaseTest.clickElement(myAccountLink);
		Reporter.log("My account link clicked successfully");
	}

}
