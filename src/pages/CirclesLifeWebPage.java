package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

import tests.BaseTest;

public class CirclesLifeWebPage {

	WebDriver driver;

    public CirclesLifeWebPage(WebDriver driver){ 
             this.driver=driver; 
    }
	
    @FindBy(how=How.XPATH, using="//a[text()='Sign up']") WebElement signUp;

	/**
	 * This method is to click sign up link in the web page
	 */
	public void clickSignUp() {
		BaseTest.driverWait.until(ExpectedConditions.titleIs("Circles.Life | Unlimit your telco. Now."));
		BaseTest.clickElement(signUp);
		Reporter.log("SignUp link clicked successfully");
	}
	
}
