package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import tests.BaseTest;

public class CirclesLifeSignUpPage {

	WebDriver driver;

	public CirclesLifeSignUpPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//a[@class='Links']//span[text()='CREATE ACCOUNT']")
	WebElement createAcctLink;
	@FindBy(how = How.XPATH, using = "//input[@name='first_name']")
	WebElement firstName;
	@FindBy(how = How.XPATH, using = "//input[@name='last_name']")
	WebElement lastName;
	@FindBy(how = How.XPATH, using = "//input[@name='email']")
	WebElement email;
	@FindBy(how = How.XPATH, using = "//input[@name='password']")
	WebElement pwd;
	@FindBy(how = How.XPATH, using = "//button[text()='Create account']")
	WebElement CreateBtn;

	/**
	 * This Method fills the create account form and submits.
	 * @param fstName
	 * @param lstName
	 * @param emailID
	 * @param password
	 */
	public void fillCreateAcctForm(String fstName, String lstName, String emailID, String password) {
		BaseTest.driverWait.until(ExpectedConditions.urlContains("https://shop.circles.life/signup"));
		createAcctLink.click();
		BaseTest.driver.navigate().refresh();
		BaseTest.fillField(firstName, fstName);
		BaseTest.fillField(lastName, lstName);
		BaseTest.fillField(email, emailID);
		BaseTest.fillField(pwd, password);
		BaseTest.clickElement(CreateBtn);
		Reporter.log("Create account submitted successfully");
	}
}
