package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import tests.BaseTest;

public class FaceBookHomePage {

	WebDriver driver;

	public FaceBookHomePage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//a[text()='Home']")
	WebElement homeLink;
	@FindBy(how = How.XPATH, using = "//a[@title='Leave a comment']")
	WebElement commentLink;
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'UFIAddCommentInput')]//div[@data-testid='ufi_comment_composer']")
	WebElement commentBox;
	@FindBy(how = How.XPATH, using = "//div[@class='UFICommentContent']//span[@class='UFICommentBody']/span")
	WebElement commentText;

	/**
	 * This method to click on home button
	 */
	public void clickOnHomeLink() {

		BaseTest.clickElement(homeLink);

	}

	/**
	 * Method to click on the comment link
	 */
	public void clickOnCommentLink() {
		BaseTest.clickElement(commentLink);
	}

	/**
	 * This method enters comments in to the field
	 * 
	 * @param comment
	 */
	public void enterComments(String comment) {

		BaseTest.clickElement(commentBox);
		BaseTest.fillField(commentBox, comment);
		BaseTest.browserWait(3000L);
		BaseTest.fillField(commentBox, Keys.ENTER);
		BaseTest.browserWait(2000L);
		Reporter.log("Comments entered successfully");
	}

	/**
	 * THis method validates the comment added with the input
	 * 
	 * @param comment
	 */
	public void validateComment(String comment) {
		List<WebElement> commentsLst = new ArrayList<>();
		commentsLst = driver
				.findElements(By.xpath("//div[@class='UFICommentContent']//span[@class='UFICommentBody']/span"));
		for (int i = 0; i >= commentsLst.size(); i++) {
			BaseTest.driverWait.until(ExpectedConditions.visibilityOf(commentsLst.get(i)));
			if (commentsLst.get(i).isDisplayed()) {
				System.out.println(commentsLst.get(i).getText());
				Assert.assertEquals(commentsLst.get(i).getText(), comment);
				Reporter.log("Comment validation successful");
			} else {
				Reporter.log("Expected comment is not listed in the comments section");
			}
		}
	}

}
